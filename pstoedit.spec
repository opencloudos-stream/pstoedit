Summary:       Translates PostScript and PDF graphics into other vector formats
Name:          pstoedit
Version:       4.00
Release:       3%{?dist}
License:       GPLv2+
URL:           http://www.pstoedit.net
Source0:       https://sourceforge.net/projects/pstoedit/files/pstoedit/%{version}/pstoedit-%{version}.tar.gz

Patch3000:     pstoedit-pkglibdir.patch
Patch3001:     pstoedit-fix-gcc12.patch

BuildRequires: make libzip-devel libEMF-devel gd-devel dos2unix ghostscript plotutils-devel gcc-c++ gcc
Requires:      ghostscript

%description
Pstoedit converts PostScript and PDF files to various vector graphic formats. The resulting files
can be edited or imported into various drawing packages. Pstoedit comes with a large set of
integrated format drivers

%package devel
Summary:        Headers for developing programs that will use %{name}
Requires:       %{name} = %{version}-%{release}

%description devel
This package contains the header files needed for developing %{name} applications


%prep
%autosetup -n %{name}-%{version} -p1

dos2unix doc/*.htm doc/readme.txt

%build

%configure \
    --disable-static \
    --enable-docs=no \
    --with-libzip-include=%{_includedir} \
    %{nil}

%make_build

%install
%make_install
mkdir -p %{buildroot}%{_mandir}/man1
install -pm 644 doc/pstoedit.1 %{buildroot}%{_mandir}/man1/
find %{buildroot} -type f -name "*.la" -exec rm -f {} ';'

%files
%license copying
%doc doc/readme.txt doc/pstoedit.htm doc/changelog.htm doc/pstoedit.pdf
%{_datadir}/pstoedit/
%{_bindir}/pstoedit
%{_libdir}/*.so.*
%{_libdir}/pstoedit/
%{_mandir}/man1/*

%files devel
%doc doc/changelog.htm
%{_includedir}/pstoedit/
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc
%{_datadir}/aclocal/*.m4


%changelog
* Thu Sep 26 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 4.00-3
- Rebuilt for clarifying the packages requirement in BaseOS and AppStream

* Fri Aug 16 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 4.00-2
- Rebuilt for loongarch release

* Wed Jan 03 2024 Upgrade Robot <upbot@opencloudos.org> - 4.00-1
- Upgrade to version 4.00

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 3.78-6
- Rebuilt for OpenCloudOS Stream 23.09

* Mon Sep 04 2023 cunshunxia <cunshunxia@tencent.com> - 3.78-5
- Rebuilt for ghostscript 10.01.2

* Thu Aug 24 2023 kianli <kianli@tencent.com> - 3.78-4
- Rebuilt for libzio 1.10.0

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 3.78-3
- Rebuilt for OpenCloudOS Stream 23.05

* Fri Mar 31 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 3.78-2
- Rebuilt for OpenCloudOS Stream 23

* Wed Feb 8 2023 Shuo Wang <abushwang@tencent.com> - 3.78-1
- initial build
